# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).
#

# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

font pango:M PLUS 1 Code regular 10

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
#bindsym $mod+Return exec /usr/bin/st -f "Hack:Regular:size=10" -c st -e tmux
#bindsym $mod+Return exec /usr/bin/st -f "Hack:Regular:size=10" -c st
#bindsym $mod+Return exec /usr/bin/st -f "M+ 1m:medium:size=12" -c st
#bindsym $mod+Return exec /usr/bin/terminology
bindsym $mod+Return exec /usr/bin/alacritty

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
# bindsym $mod+d exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
bindsym $mod+d exec --no-startup-id i3-dmenu-desktop \
                --dmenu='rofi -i -fuzzy -dmenu -modi drun -opacity 88 -p ".oO "'
bindsym XF86LaunchA exec --no-startup-id rofi -show window -opacity 88

bindsym $mod+Shift+d exec --no-startup-id spacefm

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+i focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+i move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation bindsym $mod+Shift+v split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

bindsym $mod+Control+Up workspace prev
bindsym $mod+Control+Down workspace next
bindsym $mod+Control+Left workspace prev_on_output
bindsym $mod+Control+Right workspace next_on_output

bindsym Shift+$mod+bracketright move workspace to output right
bindsym Shift+$mod+bracketleft move workspace to output left

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning \
                -m 'This will end your X session.' -b 'Exit' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym j resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 1 px or 1 ppt
        bindsym Down resize grow height 1 px or 1 ppt
        bindsym Up resize shrink height 1 px or 1 ppt
        bindsym Right resize grow width 1 px or 1 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

bindsym XF86AudioRaiseVolume exec --no-startup-id amixer sset Master 5%+ unmute
bindsym XF86AudioLowerVolume exec --no-startup-id amixer sset Master 5%- unmute
bindsym XF86AudioMute exec --no-startup-id amixer sset Master toggle

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 10
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 10

set $mode_system System (l) lock, (e) logout, (h) hibernate, (r) reboot, (s) suspend, (Shift+s) shutdown
mode "$mode_system" {
        bindsym l exec --no-startup-id i3lock -e -c 002b36, \
                mode "default"
        bindsym e exec i3-msg exit
        bindsym h exec --no-startup-id i3lock -e -c 002b36 \
                && sudo /usr/bin/ZZZ, mode "default"
        bindsym r exec sudo reboot
        bindsym s exec i3lock -e -c 002b36 \
                && sudo /usr/sbin/zzz, mode "default"
        bindsym Shift+s exec sudo shutdown -P now

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
bindsym $mod+Shift+s mode "$mode_system"

# Make the currently focused window a scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the first scratchpad window
bindsym $mod+minus scratchpad show

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        font pango:M PLUS 1 Code regular 10
        status_command i3status-rs
        tray_output eDP1
        colors {
                background #002b36
                statusline #fdf6e3
                separator  #2aa198

                focused_workspace  #2aa198 #2aa198 #fdf6e3
                active_workspace   #2aa198 #002b36 #fdf6e3
                inactive_workspace #002b36 #002b36 #fdf6e3
                urgent_workspace   #dc322f #dc322f #fdf6e3
                binding_mode       #b58900 #b58900 #fdf6e3
        }
}

# class                 border  backgr. text    indicator child_border
client.focused          #2aa198 #2aa198 #fdf6e3 #b58900   #2aa198
client.focused_inactive #073642 #073642 #93a1a1 #073642   #073642
client.unfocused        #073642 #073642 #93a1a1 #b58900   #073642
client.urgent           #dc322f #dc322f #fdf6e3 #b58900   #dc322f
client.placeholder      #b58900 #b58900 #fdf6e3 #b58900   #b58900

client.background       #859900

no_focus [class=".*"]
for_window [class=".*"] title_format "%class: %title"
for_window [class="URxvt"] exec --no-startup-id "transset -a .888888"

for_window [class="Arandr"] floating enable
for_window [class="Connman-gtk"] floating enable
for_window [class="Cssh"] floating enable
#for_window [class="feh"] floating enable
for_window [class="MPlayer"] floating enable
for_window [class="mpv"] floating enable
for_window [class="Pavucontrol"] floating enable
for_window [class="Skype" title="Options"] floating enable
for_window [class="smplayer"] floating enable
for_window [class="Steam"] floating enable
#for_window [class="Virt-manager"] floating enable
for_window [class="VirtualBox"] floating enable
for_window [class="vlc"] floating enable

exec_always --no-startup-id dunst
exec_always --no-startup-id nitrogen --restore
#exec_always --no-startup-id compton -f
exec_always --no-startup-id ~/.cargo/bin/inputplug \
		-c ~/.local/bin/config-input
exec_always --no-startup-id /usr/sbin/daemonize -p /tmp/.xautolock.$USER.pid \
		-l /tmp/.xautolock.$USER.lock /usr/bin/xautolock -time 5 -notify 10 \
		-locker "i3lock -e -c 002b36"
exec_always --no-startup-id eval $(keychain -q --eval --agents ssh,gpg)
exec_always --no-startup-id nm-applet
exec_always --no-startup-id /usr/bin/parcellite
exec --no-startup-id echo 1 | sudo tee \
		/sys/module/hid_apple/parameters/swap_opt_cmd

exec_always --no-startup-id pipewire
exec_always --no-startup-id xinput disable "ELAN Touchscreen"
